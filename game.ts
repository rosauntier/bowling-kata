export type Frame = number[];

function sum(...nums: number[] | number[][]) {
  return nums.flat().reduce((a, b) => a + b);
}

function throwValidationError(frame: Frame): never {
  throw new Error(`invalid frame ${JSON.stringify(frame)}`);
}

function isStrike(frame: Frame) {
  return frame.length === 1 && frame[0] === 10;
}

function isSpare(frame: Frame) {
  return frame.length === 2 && frame[0] < 10 && sum(frame) === 10;
}

function isNormal(frame: Frame) {
  return frame.length === 2 && sum(frame) < 10;
}

function validateLastFrame(frame: Frame) {
  const firstTwoThrows = [frame[0], frame[1]];
  return (isNormal(firstTwoThrows) && frame.length === 2) ||
    (isSpare(firstTwoThrows) && frame.length === 3) ||
    sum(frame) === 30 ||
    throwValidationError(frame);
}

function validateRegularFrame(frame: Frame) {
  return isStrike(frame) ||
    isSpare(frame) ||
    isNormal(frame) ||
    throwValidationError(frame);
}

function handleEdgecases(frame: Frame) {
  if (!frame.length) throwValidationError(frame);
  if (frame.some((num) => num < 0)) throwValidationError(frame);
}

function getFrameScore(frame: Frame, otherBowls: number[]) {
  const frameScore = sum(frame);

  if (isStrike(frame)) return sum(frameScore, otherBowls[0], otherBowls[1]);

  if (isSpare(frame)) return sum(frameScore, otherBowls[0]);

  return frameScore;
}

function calculateScore(frames: Frame[], score = 0) {
  const [frame, ...others] = frames;

  if (frames.length === 1) {
    return sum(score, ...frame);
  }

  return calculateScore(
    others,
    sum(score, getFrameScore(frame, others.flat())),
  );
}

function validateGame(frames: Frame[]) {
  if (frames.length !== 10) throw new Error("need 10 frames");
  frames.forEach((frame, idx) =>
    validateFrame(frame, idx === 9) || throwValidationError(frame)
  );
}

export function validateFrame(frame: Frame, isLast = false) {
  handleEdgecases(frame);
  if (isLast) return validateLastFrame(frame);
  return validateRegularFrame(frame);
}

export function getScore(frames: Frame[]) {
  validateGame(frames);
  return calculateScore(frames);
}
