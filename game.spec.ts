import { describe, expect, test } from "vitest";
import { type Frame, getScore, validateFrame } from "./game.ts";

function assertRegularFrameValid(frame: Frame) {
  expect(validateFrame(frame))
    .toBe(true);
}
function assertRegularFrameInvalid(frame: Frame) {
  expect(() => validateFrame(frame))
    .toThrow();
}
function assertLastFrameValid(frame: Frame) {
  expect(validateFrame(frame, true))
    .toBe(true);
}
function assertLastFrameInvalid(frame: Frame) {
  expect(() => validateFrame(frame, true))
    .toThrow();
}
function assertEdgeCase(frame: Frame) {
  assertRegularFrameInvalid(frame);
  assertLastFrameInvalid(frame);
}
describe("validate a frame", () => {
  test("regular frame", () => {
    assertRegularFrameValid([0, 0]);
    assertRegularFrameValid([0, 10]);
    assertRegularFrameValid([3, 7]);
    assertRegularFrameValid([3, 6]);
    assertRegularFrameValid([10]);

    assertRegularFrameInvalid([10, 0]);
    assertRegularFrameInvalid([10, 10]);
    assertRegularFrameInvalid([8]);
    assertRegularFrameInvalid([]);
    assertRegularFrameInvalid([0, 0, 0]);
    assertRegularFrameInvalid([0, 10, 0]);
  });

  test("last frame", () => {
    assertLastFrameValid([0, 0]);
    assertLastFrameValid([0, 8]);
    assertLastFrameValid([8, 0]);
    assertLastFrameValid([7, 3, 0]);
    assertLastFrameValid([0, 10, 0]);
    assertLastFrameValid([0, 10, 10]);
    assertLastFrameValid([10, 10, 10]);

    assertLastFrameInvalid([0, 0, 0]);
    assertLastFrameInvalid([10]);
    assertLastFrameInvalid([]);
    assertLastFrameInvalid([10, 7, 0]);
    assertLastFrameInvalid([6, 5]);
    assertLastFrameInvalid([6, 5, 0]);
  });

  test("edgecases", () => {
    assertEdgeCase([]);
    assertEdgeCase([0, 0, 0, 0]);
    assertEdgeCase([-1]);
    assertEdgeCase([7, -1]);
  });
});

describe("scoring", () => {
  test("score only games with 10 frames", () => {
    expect(() => getScore([])).toThrowError("need 10 frames");
  });
  test("only zeros", () => {
    expect(getScore([
      [0, 0],
      [0, 0],
      [0, 0],
      [0, 0],
      [0, 0],
      [0, 0],
      [0, 0],
      [0, 0],
      [0, 0],
      [0, 0],
    ])).toBe(0);
  });
  test("perfect game", () => {
    expect(getScore([
      [10],
      [10],
      [10],
      [10],
      [10],
      [10],
      [10],
      [10],
      [10],
      [10, 10, 10],
    ])).toBe(300);
  });
  test("all spares", () => {
    expect(getScore([
      [5, 5],
      [5, 5],
      [5, 5],
      [5, 5],
      [5, 5],
      [5, 5],
      [5, 5],
      [5, 5],
      [5, 5],
      [5, 5, 5],
    ])).toBe(150);
  });
  test("invalid game", () => {
    expect(() =>
      getScore([
        [10, 10],
        [10],
        [10],
        [10],
        [10],
        [10],
        [10],
        [10],
        [10],
        [10, 10, 10],
      ])
    ).toThrow();
  });
  test("sample game", () => {
    expect(getScore([
      [0, 1],
      [2, 8],
      [3, 4],
      [10],
      [6, 3],
      [10],
      [3, 7],
      [4, 2],
      [1, 2],
      [0, 10, 10],
    ])).toBe(112);
  });
});
